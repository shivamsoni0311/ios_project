//
//  RestaurantMapViewController.swift
//  InClassExercisesStarter
//
//  Created by parrot on 2018-11-22.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MapKit

class RestaurantMapViewController: UIViewController, MKMapViewDelegate{

    // MARK: Outlets
    @IBOutlet weak var mapView: MKMapView!
    
   
     // Objects of Pokemon
    // 1. create your pokemon objects here
    var pikachu = Pokemon()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("loaded the map screen")
        
        let x = CLLocationCoordinate2DMake(43.6532, -79.3832)
        
        let y = MKCoordinateSpanMake(0.1, 0.1)
        
        let z = MKCoordinateRegionMake(x, y)
        self.mapView.setRegion(z, animated: true)
        
        let pin1 = MKPointAnnotation()
        pin1.coordinate = CLLocationCoordinate2DMake(43.722020, -79.256780)
        self.mapView.addAnnotation(pin1)
        
        let pin2 = MKPointAnnotation()
        pin2.coordinate = CLLocationCoordinate2DMake(43.764432, -79.317917)
        self.mapView.addAnnotation(pin2)
        
        let pin3 = MKPointAnnotation()
        pin3.coordinate = CLLocationCoordinate2DMake(43.649101, -79.395929)
        self.mapView.addAnnotation(pin3)
        
        let pin4 = MKPointAnnotation()
        pin4.coordinate = CLLocationCoordinate2DMake(43.578846, -79.616384)
        self.mapView.addAnnotation(pin4)
        
        let pin5 = MKPointAnnotation()
        pin5.coordinate = CLLocationCoordinate2DMake(43.663946, -79.731980)
        self.mapView.addAnnotation(pin5)
        
        
      
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    // MARK: Actions

    @IBAction func zoomInPressed(_ sender: Any) {
        
       // print("zoom in!")
        
        // HINT: Check MapExamples/ViewController.swift
        
        var map = mapView.region
        
        
        
        map.span.latitudeDelta = map.span.latitudeDelta / 4
        
        map.span.longitudeDelta = map.span.longitudeDelta / 4
        
        
        
        self.mapView.setRegion(map, animated: true)
    }
    
    @IBAction func zoomOutPressed(_ sender: Any) {
        // zoom out
        //print("zoom out!")
        
        // HINT: Check MapExamples/ViewController.swift
        
        
        var map = mapView.region
        
        map.span.latitudeDelta = map.span.latitudeDelta * 2
        
        map.span.longitudeDelta = map.span.longitudeDelta * 2
        
        self.mapView.setRegion(map, animated: true)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    // Actions Buttons for fight
    @IBAction func btnPikachu(_ sender: UIButton) {
       
        
        // 2. set the properties of your pokemon objects here
        pikachu.name = "Pikachu"
        pikachu.hp = 100
        pikachu.attack = 10
        
        performSegue(withIdentifier: "segPikachu", sender: self)
    }
    
    @IBAction func btnCharminder(_ sender: UIButton) {
    }
    
    @IBAction func btnJigglypuff(_ sender: UIButton) {
    }
    @IBAction func btnSquirtle(_ sender: UIButton) {
    }
    @IBAction func btnBulbasaur(_ sender: UIButton) {
    }
    
    override func prepare(for segue : UIStoryboardSegue, sender: Any? ){
        
        let g = segue.destination as! FightViewController
        
        g.pokemon = pikachu
        
    }
    
}
