//
//  FightViewController.swift
//  InClassExercisesStarter
//
//  Created by shivam on 2018-12-06.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit

class FightViewController: UIViewController {

    var pokemon = Pokemon()
    
    
    @IBOutlet weak var PokemonDetails: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        PokemonDetails.text = pokemon.name
        
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
